var dnode = require('dnode');
var net = require('net');

var d = dnode();

d.on('remote', function (remote) {
    remote.setCurrentGame('Real', 'Atletico', function(err, id) {
        console.log('set current game ' + id);
    });

    setTimeout(function() {
        console.log('adding question');
        remote.addQuestion({
            text: 'What is this shit?',
            answers: ['yes', 'no'],
            timeout: 30
        }, function(err) {
            if(err) return console.err(err);

            console.error('added question!');
        });
    }, 5000);
});

var c = net.connect('/tmp/hacksports.sock');
c.pipe(d).pipe(c);
