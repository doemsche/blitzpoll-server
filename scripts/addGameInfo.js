#!/usr/bin/env node

var Game = require('../lib/game');
var game = new Game();

game.setCurrentGame({
    home: 'Real Madrid',
    away: 'Atletico Madrid',
    date: '2014-05-24'
}, function(err, id) {
    if(err) return console.error(err);

    console.log('it worked: ' + id);
});
