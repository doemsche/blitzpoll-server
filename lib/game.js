var db = require('./level-db');
var gameDataDb = db('./db/game-db');
var uuid = require('uuid');

function Game() {}

Game.prototype.setCurrentGame = function(game, cb) {
    game.id = uuid.v1();
    gameDataDb.put('currentGame', game, function(err) {
        if(err) cb(err);

        cb(null, game.id);
    })
}

Game.prototype.getCurrentGame = function(cb) {
    gameDataDb.get('currentGame', function(err, data) {
        if(err) console.error(err);

        cb(data);
    });
}

module.exports = Game;
