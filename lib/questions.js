var inherits = require('util').inherits;
var EventEmitter = require('events').EventEmitter;
var LiveStream = require('level-live-stream');
var db = require('./level-db');
var Answers = require('./answers');
var questionDb = db('./db/question-db');
var uuid = require('uuid');
var concat = require('concat-stream');

inherits(Questions, EventEmitter);

function Questions() {
    EventEmitter.call(this);

    this.answers = new Answers();

    LiveStream.install(questionDb);
    var liveStream = questionDb.liveStream();
    liveStream.on('data', function(operation) {
        // Create live-feed for answers
        var feed = this.answers.getLiveFeed(operation.key);

        feed.on('data', function(operation) {
            this.answers.results[operation.value.questionId][operation.value.answer]++;
        }.bind(this));

        // distribute question
        var obj = operation.value;
        obj.id = operation.key;

        this.answers.results[obj.id] = {};

        obj.answers.forEach(function(answer) {
            this.answers.results[obj.id][answer] = 0;
        }.bind(this));

        this.emit('distributeQuestion', obj);

        var iv = setInterval(function() {
            this.emit('distributeResults', {
                questionId: obj.id,
                answers: this.answers.results[obj.id]
            });
        }.bind(this), 2000);

        setTimeout(function() {
            feed.close();
            clearInterval(iv);
        }, obj.timeout * 1000 + 2000);
    }.bind(this));

    this.on('addAnswer', function(answer) {
        var questionId = answer.questionId;
        this.answers.addAnswer(questionId, answer);
    }.bind(this));
}

Questions.prototype.addQuestion = function(currentGameId, question, cb) {
    question.timeout = parseInt(question.timeout);
    questionDb.put(currentGameId + '-' + uuid.v1(), question, function(err) {
        if(err) return cb(err);

        cb(null);
    });
}

Questions.prototype.getPreviousQuestions = function(gameId, cb) {
    questionDb.createReadStream({
        start: gameId + '-',
        end: gameId + '-~'
    }).pipe(concat(function(rows) {
        cb(rows.map(function(row) {
            var obj = row.value;
            obj.id = row.key;
            obj.results = this.answers.results[obj.id];
            return obj;
        }.bind(this)));
    }.bind(this)));
}

module.exports = Questions;
