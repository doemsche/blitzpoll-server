var db = require('./level-db');
var livefeed = require('level-livefeed');
var uuid = require('uuid');

var answerDataDb = db('./db/answer-db');

function Answers() {
    this.results = {};
}

Answers.prototype.getLiveFeed = function(questionId) {
    var stream = livefeed(answerDataDb, {
        start: questionId + '-',
        end: questionId + '-~'
    });

    return stream;
}

Answers.prototype.addAnswer = function(questionId, answer) {
    answerDataDb.put(questionId + '-' + uuid.v1(), answer, function(err) {
        if(err) console.error(err);
    });
}

module.exports = Answers;
