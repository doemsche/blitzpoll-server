var globals = require('../globals');

module.exports = function(game, questions) {
    return {
        'addQuestion': function(question, cb) {
            question.timestamp = Date.now();
            questions.addQuestion(globals.currentGameId, question, function(err) {
                if(err) return cb(err);

                cb(null);
            });
        },
        'setCurrentGame': function(home, away, cb) {
            game.setCurrentGame({
                home: home,
                away: away,
                date: new Date()
            }, function(err, id) {
                if(err) return cb(err);

                globals.sendNewGame();
                globals.currentGameId = id;
                return cb(null, id);
            });
        },
        'getPreviousQuestions': function(cb) {
            questions.getPreviousQuestions(currentGameId, cb);
        }
    }
};
