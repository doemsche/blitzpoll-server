var levelup = require('levelup');

module.exports = function(path) {
    var db = levelup(path, {
        keyEncoding: require('bytewise'),
        valueEncoding: 'json'
    });

    return db;
}
