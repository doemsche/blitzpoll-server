# hacksports-server

This is the backend server for our [#hacksports](https://twitter.com/search?q=%23hacksports) app.

## Data

The data is being streamed over a websocket (via [shoe](https://www.npmjs.org/package/shoe)).

### Format

 Type     | Format
----------|--------------------
QUESTION: |{ id: string, text: string, answers: array, timeout: int, image: string }
GOAL:     |{ for: string ("home"/"away"), scorer: string, time: timestamp }
ANSWER:   |{ questionId: string, answer: string }
ANSWERS:  |{ questionId: string, answers: object }
