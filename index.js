var shoe = require('shoe');
var http = require('http');
var st = require('st');
var inspect = require('util').inspect;
var Questions = require('./lib/questions');
var Game = require('./lib/game');
var dnode = require('dnode');
var net = require('net');
var url = require('url');
var fs = require('fs');
var globals = require('./globals');

var game = new Game();
var questions = new Questions();

game.getCurrentGame(function(game) {
    if(game) globals.currentGameId = game.id;
}.bind(this));

// listen on unix socket
var server = net.createServer(function (c) {
    var d = dnode(require('./lib/journalist-interface')
        (game, questions));
    c.pipe(d).pipe(c);
});
server.listen('/tmp/hacksports.sock');

// Serve static files
var mount = st({
    path: __dirname + '/static',
    url: '/',
    index: 'index.html',
    cache: false
});
var server = http.createServer(function(req, res) {
    if(url.parse(req.url).path.indexOf('question') > -1) {
        fs.createReadStream(__dirname + '/static/index.html').pipe(res);
        return;
    }

    mount(req, res);
});
server.listen(process.env.PORT);

var allStreams = {};

questions.on('distributeQuestion', function(question) {
    Object.keys(allStreams).forEach(function(key) {
        var stream = allStreams[key];
        stream.write('QUESTION: ' + JSON.stringify(question));
    });
});

questions.on('distributeResults', function(answers) {
    Object.keys(allStreams).forEach(function(key) {
        var stream = allStreams[key];
        stream.write('ANSWERS: ' + JSON.stringify(answers));
    });
});

globals.sendNewGame = function() {
    Object.keys(allStreams).forEach(function(key) {
        var stream = allStreams[key];
        stream.write('NEW_GAME: ' + JSON.stringify({}));
    });
}

var sock = shoe(function (stream) {
    allStreams[stream.id] = stream;

    stream.on('data', function(data) {
        var cmd = data.substring(0, data.indexOf(':'));
        var obj = JSON.parse(data.substring(data.indexOf(':') + 1).trim());

        switch(cmd) {
        case 'ANSWER': questions.emit('addAnswer', obj); break;
        case 'GET_GAME_INFO': game.getCurrentGame(function(game) {
            if(!game) game = {};
            stream.write('GAME_INFO: ' + JSON.stringify(game));
        });
        break;
        case 'GET_PREVIOUS_QUESTIONS': questions.getPreviousQuestions(globals.currentGameId, function(questions) {
            stream.write('PREVIOUS_QUESTIONS: ' + JSON.stringify(questions));
        });
        break;
        }
    }.bind(this));

    stream.on('end', function () {
        delete allStreams[stream.id];
    });
}.bind(this));
sock.install(server, '/live-data');

console.log('listening on port ' + process.env.PORT);
